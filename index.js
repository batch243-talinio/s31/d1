// use the "require" directive to load the HTTP module of node JS
// a "module" is software component or part of a program that containse one or more routines
// the 'http module' lets Node.js transfer data using the HTTP (HyperText Transfer Protocol)
// HTTP is a protocol that allows the fetching of resources such as html documents.
// Clients (browser) and severs (nodeJs/ExpressJs apps) communicate by exchanging messages.

// REQUEST - messages sent by the client (ussualy in a Web browser)
// RESPONSES - messages sent by the server as an answer to the client

let http = require("http");


// Creating a Server
	/*
		- The http server has a createServer()method that accepts a function as an argument and allows server creation.

		- the argument passed in the function are request & response objects (data type) that contain methods that allow us to recieve request from the client and send the response back.

		- Using the module's createServer() Method, we can create an http server that listens to the requests on a specified port and gives back to the client.
	*/

	// Define the port number that the server will be listening

	// A port is virtual where network connections start and end

	// send a response back to client 
	// use the writeHead() method in order to set a status code
	// set a status code for the reponse - 200 - which means OK!

		http.createServer(function(request, response){
				response.writeHead(200,{'Content-Type':'text/plain'})
				response.end('Hello ZawarudOooo!')
		}).listen(4000)

		console.log("Server running at localhosts")

		/*
			IMPORTANT!
				- installing the package will allow the server to automatically restart when files have been changed or updated.
				- '-g' refers to a global installation where the current version of the package/dependency will be installed locally on our device allowing us to use nodemon on any prokect.
		*/