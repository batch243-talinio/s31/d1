/*
	CREATE a ROUTE
		"/greeting"
*/
const http = require("http");
const port = 4000


const server = http.createServer((request, response) => 
	{
		// accessing the 'greeting' route returns a message of 'Hello World'
		if(request.url == '/greeting'){
			response.writeHead(200, {'Content-Type':'text/plain'})
			response.end('Hello again.')

		// accessing the 'homepage' route returns a message of ....
		}else if(request.url == '/homepage'){
			response.writeHead(200,{'Content-Type': 'text/plain'})
			response.end("Welcome home")

		// all other routes will return a message of 'Page not Available'
		}else{
			response.writeHead(400,{'Content-Type': 'text/plain'})
			response.end("Page not Available")
		}
	}
)

// uses the 'server' and 'port' variables created above
server.listen(port)
// when the server is running, console will print this message:
console.log(`Server now accessible at localhosts: ${port}`)